const models = require('../models');

const convertLeadToApplicant = async (leadId) => {
    const record = await models.Employee.fetchById(models, leadId);   

    if(!record) {
        throw { status: 422, message: "This record does not exist" }
    }

    const applicantStatus = await models.Status.findOne({ where: { name: "Applicant" } });
    
    await record.addStatuses([applicantStatus.id]);    
    return true;
}

const convertApplicantToCandidate = async (id) => {
    const applicant = await models.Employee.fetchByMaxStatus(models, id);
    
    if(!applicant) {
        throw { status: 422, message: "This record does not exist" }
    }
    
    if(!applicant.Statuses || !applicant.Statuses[0] || applicant.Statuses[0].dataValues.name !== "Applicant") {
        throw { status: 422, message: "This record must be an Applicant" }        
    }

    const documents = await models.Document.findAll({ where: { employeeId: id } });
    
    if(!documents[0]) {
        throw { status: 422, message: "This record must have at least one document associated" }  
    }

    const candidateStatus = await models.Status.findOne({ where: { name: "Candidate" } });
    await applicant.addStatuses([candidateStatus.id]);

    return true;            
}

const convertCandidateToEmployee = async (id, date) => {
    let hireDate = null;
    
    if(!date) {
        throw { status: 400, message: "A Hired Date must be provided" }
    }

    try{
        hireDate = new Date(date);
    } catch(error) {
        throw { status: 400, message: "A valid Hired Date must be provided" }
    }

    const candidate = await models.Employee.fetchByMaxStatus(models, id);

    if(!candidate) {
        throw { status: 422, message: "This record does not exist" }
    }
    
    if(!candidate.Statuses || !candidate.Statuses[0] || candidate.Statuses[0].dataValues.name !== "Candidate") {
        throw { status: 422, message: "This record must be a Candidate" }        
    }

    const t = await models.sequelize.transaction();

    try {
        const [, [edited]] = await models.Employee.update({ hireDate: date }, { returning: true, where: { id } }, { transaction: t });
        const employeeStatus = await models.Status.findOne({ where: { name: "Employee" } });
        
        await edited.addStatuses([employeeStatus.id], { transaction: t });
        await t.commit();

        return true
    } catch (error) {
        console.log(error);
        await t.rollback();
        throw { status: 500, message: "Failed to convert Candidate to Employee" }
    }
}

module.exports = { convertLeadToApplicant, convertApplicantToCandidate, convertCandidateToEmployee };