'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('EmployeeStatus', {
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      employeeId: {
        type: Sequelize.INTEGER,
        primaryKey: true
      },
      statusId: {
        type: Sequelize.INTEGER,
        primaryKey: true
      }      
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('EmployeeStatus');
  }
};
