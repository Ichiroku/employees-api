'use strict';
module.exports = (sequelize, DataTypes) => {
  const Status = sequelize.define('Status', {
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },

    description: DataTypes.STRING,
    isActive: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    }
  }, {});

  Status.associate = function(models) {
    Status.belongsToMany(models.Employee, { through: 'EmployeeStatus', foreignKey: "statusId" });
  };

  return Status;
};