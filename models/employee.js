'use strict';
module.exports = (sequelize, DataTypes) => {
  const Employee = sequelize.define('Employee', {
    firstName: {
      type: DataTypes.STRING,
      allowNull: false
    },

    lastName: {
      type: DataTypes.STRING,
      allowNull: false
    },

    hireDate: DataTypes.DATE,

    isActive: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    }
  }, {});

  Employee.associate = function(models) {
    Employee.hasMany(models.Document, { foreignKey: "employeeId" });
    Employee.belongsToMany(models.Status, { through: 'EmployeeStatus', foreignKey: "employeeId" });
  };

  Employee.fetchAll = async (models) => {
    const records = await Employee.findAll({ 
      include: {

        model: models.Status,
        order: [['createdAt', 'DESC']]
      }
    });
    
    return records;    
  }

  Employee.fetchById = async (models, id) => {
    const records = await Employee.findOne({ 
      where: { id },
      include: {
        model: models.Status        
      }
    });

    return records;
  }

  Employee.fetchById = async (models, id) => {
    const records = await Employee.findOne({ 
      where: { id },
      include: {
        model: models.Status        
      }
    });
    
    return records;    
  }

  Employee.fetchByMaxStatus = async(models, id) => {
    const records = await Employee.findOne({ 
      where: { id },
      order: [[models.Status, 'id', 'DESC']],
      include: {
        model: models.Status        
      }
    });

    return records;
  }

  Employee.editRecord = async(id, changes, transactionConfig = {}) => {
    return Employee.update(changes, {
      where: { id }
    }, transactionConfig);
  }
  
  return Employee;
};