'use strict';
module.exports = (sequelize, DataTypes) => {
  const Document = sequelize.define('Document', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    
    description: DataTypes.STRING
  }, {});

  Document.associate = function(models) {
    Document.belongsTo(models.Employee, { foreignKey: "employeeId" });
  };

  return Document;
};