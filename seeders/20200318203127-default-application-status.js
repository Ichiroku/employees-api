'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('EmployeeStatus', [
      { employeeId: 1, statusId: 1, createdAt: new Date(), updatedAt: new Date() },
      { employeeId: 2, statusId: 2, createdAt: new Date(), updatedAt: new Date() },
      { employeeId: 3, statusId: 3, createdAt: new Date(), updatedAt: new Date() },
      { employeeId: 4, statusId: 4, createdAt: new Date(), updatedAt: new Date() },
      { employeeId: 5, statusId: 1, createdAt: new Date(), updatedAt: new Date() }
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('EmployeeStatus', null, {});
  }
};
