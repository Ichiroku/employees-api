'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Statuses', [
      { name: "Lead", description: "Lead", isActive: true, createdAt: new Date(), updatedAt: new Date() },
      { name: "Applicant", description: "Applicant", isActive: true, createdAt: new Date(), updatedAt: new Date() },
      { name: "Candidate", description: "Candidate", isActive: true, createdAt: new Date(), updatedAt: new Date() },
      { name: "Employee", description: "Employee", isActive: true, createdAt: new Date(), updatedAt: new Date() }
    ])
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Statuses', null, {});
  }
};
