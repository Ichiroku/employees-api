'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Employees', [
      { firstName: "Jorge", lastName: "Corea", isActive: true, createdAt: new Date(), updatedAt: new Date() },
      { firstName: "John", lastName: "Doe", isActive: true, createdAt: new Date(), updatedAt: new Date() },
      { firstName: "Tonho", lastName: "Loupez", isActive: true, createdAt: new Date(), updatedAt: new Date() },
      { firstName: "Mari", lastName: "Robelo", isActive: true, createdAt: new Date(), updatedAt: new Date() },
      { firstName: "John", lastName: "Smith", isActive: true, createdAt: new Date(), updatedAt: new Date() },
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Employees', null, {});
  }
};
