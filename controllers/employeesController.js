const express = require("express");
const router = express.Router();
const wrapper = require('../middlewares/errorWrapper');
const employeeService = require('../services/employeeService');

router.post('/convert/applicant', wrapper(async (req, res) => {
    const employees = await employeeService.convertLeadToApplicant(req.body.employee);
    return res.status(200).send({ data: employees });
}));

router.post('/convert/candidate', wrapper(async (req, res) => {
    const employees = await employeeService.convertApplicantToCandidate(req.body.employee);
    return res.status(200).send({ data: employees });
}));

router.post('/convert/employee', wrapper(async (req, res) => {
    const employees = await employeeService.convertCandidateToEmployee(req.body.employee, req.body.date);
    return res.status(200).send({ data: employees });
}));

module.exports = router;